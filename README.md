# 毕设：基于Spring Boot + Vue的若依后台管理系统

## 项目简介

本项目是一个基于Spring Boot和Vue.js开发的若依后台管理系统，适用于期末大作业或毕业设计。系统集成了前后端分离的架构，提供了丰富的功能模块，帮助开发者快速搭建一个功能完善的后台管理系统。

## 功能特点

- **用户管理**：支持用户注册、登录、权限管理等功能。
- **角色管理**：可以创建不同的角色并分配相应的权限。
- **菜单管理**：自定义菜单结构，支持多级菜单。
- **日志管理**：记录系统操作日志，便于追踪和审计。
- **数据统计**：提供基本的数据统计功能，支持图表展示。

## 技术栈

- **前端**：Vue.js、Element UI
- **后端**：Spring Boot、MyBatis
- **数据库**：MySQL
- **缓存**：Redis
- **安全**：Spring Security

## 快速开始

### 环境要求

- JDK 1.8 或更高版本
- Maven 3.x
- Node.js 12.x 或更高版本
- MySQL 5.7 或更高版本
- Redis 5.x 或更高版本

### 安装步骤

1. **克隆项目**

   ```bash
   git clone https://github.com/your-repo/ruoyi-admin.git
   ```

2. **配置数据库**

   在`application.yml`文件中配置MySQL数据库连接信息。

3. **启动后端服务**

   进入项目根目录，执行以下命令启动Spring Boot应用：

   ```bash
   mvn spring-boot:run
   ```

4. **启动前端服务**

   进入`ruoyi-ui`目录，执行以下命令启动Vue.js应用：

   ```bash
   npm install
   npm run serve
   ```

5. **访问系统**

   打开浏览器，访问`http://localhost:8080`，即可进入若依后台管理系统。

## 贡献指南

欢迎大家贡献代码，提出问题或建议。请遵循以下步骤：

1. Fork本仓库。
2. 创建新的分支 (`git checkout -b feature/your-feature`)。
3. 提交你的修改 (`git commit -am 'Add some feature'`)。
4. 推送到分支 (`git push origin feature/your-feature`)。
5. 创建一个新的Pull Request。

## 许可证

本项目采用MIT许可证，详情请参阅[LICENSE](LICENSE)文件。

## 联系我们

如有任何问题或建议，请通过[issues](https://github.com/your-repo/ruoyi-admin/issues)联系我们。

---

感谢使用本项目，祝你顺利完成毕业设计或期末大作业！